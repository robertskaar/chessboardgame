package Application;

import Domain.*;
import javafx.fxml.FXML;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.layout.AnchorPane;
import javafx.scene.paint.Color;


public class Controller {

    @FXML
    Canvas gameBoard;
    @FXML
    Button startGame;
    @FXML
    AnchorPane anchorPane;
    @FXML
    Button doMove;
    @FXML
    TextField moveTo;
    @FXML
    TextField moveFrom;
    @FXML
    Canvas deadField;

    private Board chessBoard;
    private Player player1;
    private Player player2;
    private int deadx = 0;
    private int deady = 0;
    private boolean deadDidRun = false;

    public void startAGame() {
        createBoard();
        addPlayers();
    }

    private void addPlayers() {
        GraphicsContext gc = gameBoard.getGraphicsContext2D();

        player1 = new Player(Color.WHITE);
        player2 = new Player(Color.BLACK);
        Image whitePieces;
        Image blackPieces;
        int squarePositionX = 0;
        int squarePositionY = 0;

        for (int i = 0; i < 16; i++) {
            int xposition1 = player1.getPieces()[i].getXPosition();
            int yposition1 = player1.getPieces()[i].getYPosition();
            int xposition2 = player2.getPieces()[i].getXPosition();
            int yposition2 = player2.getPieces()[i].getYPosition();

            blackPieces = player2.getPieces()[i].getImage();
            gc.drawImage(blackPieces, xposition2 * 75, yposition2 * 75);

            whitePieces = player1.getPieces()[i].getImage();
            gc.drawImage(whitePieces, xposition1 * 75, yposition1 * 75);
        }
    }

    private void createBoard() {
        GraphicsContext gc = gameBoard.getGraphicsContext2D();
        chessBoard = new Board();
        int squarePositionX = 0;
        int squarePositionY = 0;


        for (int i = 0; i < 8; i++) {
            for (int j = 0; j < 8; j++) {
                gc.setFill(chessBoard.getSquares()[i][j].getColor());
                gc.fillRect(squarePositionX, squarePositionY, chessBoard.getSquares()[i][j].getSIZE(), chessBoard.getSquares()[i][j].getSIZE());
                squarePositionX += 75;
            }
            squarePositionY += 75;
            squarePositionX = 0;
        }
    }

    public void movePieces() {

        GraphicsContext gc = gameBoard.getGraphicsContext2D();
        String initialFrom = moveFrom.getText();
        String initialTo = moveTo.getText();

        String xFrom = initialFrom.substring(0, 1);

        int yFrom = Integer.parseInt(initialFrom.substring(1, 2));
        int newXFrom = -1;
        int newYFrom = -1;
        if (xFrom.equalsIgnoreCase("a")) {
            newXFrom = 0;
        } else if (xFrom.equalsIgnoreCase("b")) {
            newXFrom = 1;
        } else if (xFrom.equalsIgnoreCase("c")) {
            newXFrom = 2;
        } else if (xFrom.equalsIgnoreCase("d")) {
            newXFrom = 3;
        } else if (xFrom.equalsIgnoreCase("e")) {
            newXFrom = 4;
        } else if (xFrom.equalsIgnoreCase("f")) {
            newXFrom = 5;
        } else if (xFrom.equalsIgnoreCase("g")) {
            newXFrom = 6;
        } else if (xFrom.equalsIgnoreCase("h")) {
            newXFrom = 7;
        }

        if (yFrom == 8) {
            newYFrom = 0;
        } else if (yFrom == 7) {
            newYFrom = 1;
        } else if (yFrom == 6) {
            newYFrom = 2;
        } else if (yFrom == 5) {
            newYFrom = 3;
        } else if (yFrom == 4) {
            newYFrom = 4;
        } else if (yFrom == 3) {
            newYFrom = 5;
        } else if (yFrom == 2) {
            newYFrom = 6;
        } else if (yFrom == 1) {
            newYFrom = 7;
        }


        String xTo = initialTo.substring(0, 1);
        int yTo = Integer.parseInt(initialTo.substring(1, 2));
        int newXTo = -1;
        int newYTo = -1;
        if (xTo.equalsIgnoreCase("a")) {
            newXTo = 0;
        } else if (xTo.equalsIgnoreCase("b")) {
            newXTo = 1;
        } else if (xTo.equalsIgnoreCase("c")) {
            newXTo = 2;
        } else if (xTo.equalsIgnoreCase("d")) {
            newXTo = 3;
        } else if (xTo.equalsIgnoreCase("e")) {
            newXTo = 4;
        } else if (xTo.equalsIgnoreCase("f")) {
            newXTo = 5;
        } else if (xTo.equalsIgnoreCase("g")) {
            newXTo = 6;
        } else if (xTo.equalsIgnoreCase("h")) {
            newXTo = 7;
        }

        if (yTo == 8) {
            newYTo = 0;
        } else if (yTo == 7) {
            newYTo = 1;
        } else if (yTo == 6) {
            newYTo = 2;
        } else if (yTo == 5) {
            newYTo = 3;
        } else if (yTo == 4) {
            newYTo = 4;
        } else if (yTo == 3) {
            newYTo = 5;
        } else if (yTo == 2) {
            newYTo = 6;
        } else if (yTo == 1) {
            newYTo = 7;
        }


        int x;
        int y;
        Image moveImg = null;
        boolean moved = false;

        checkToPosition(gc, newXTo, newYTo, player1);

        checkToPosition(gc, newXTo, newYTo, player2);

        for (int i = 0; i < 16; i++) {
            y = player1.getPieces()[i].getYPosition();
            x = player1.getPieces()[i].getXPosition();
            updateToPosition(gc, newXFrom, newYFrom, newXTo, newYTo, x, y, i, player1);
        }

        for (int i = 0; i < 16; i++) {
            x = player2.getPieces()[i].getXPosition();
            y = player2.getPieces()[i].getYPosition();
            updateToPosition(gc, newXFrom, newYFrom, newXTo, newYTo, x, y, i, player2);
        }
    }

    private void updateToPosition(GraphicsContext gc, int newXFrom, int newYFrom, int newXTo, int newYTo, int x, int y, int i, Player player2) {
        Image moveImg;
        if (y==newYFrom){
            if (x==newXFrom){
                moveImg = player2.getPieces()[i].getImage();
                if (player2.getPieces()[i].getMoves()>=Math.abs(newYFrom-newYTo)) {
                    if (player2.getPieces()[i].getMoves()>=Math.abs(newXFrom-newXTo)) {
                        player2.getPieces()[i].setXPosition(newXTo);
                        player2.getPieces()[i].setYPosition(newYTo);
                        gc.setFill(chessBoard.getSquares()[newXFrom][newYFrom].getColor());
                        gc.fillRect(newXFrom * 75, newYFrom * 75, 75, 75);
                        gc.drawImage(moveImg, newXTo * 75, newYTo * 75);
                    }
                }
            }
        }
    }

    private void checkToPosition(GraphicsContext gc, int newXTo, int newYTo, Player player1) {
        int y;
        int x;
        Image moveImg;
        for (int i = 0; i < 16 ; i++) {
            y = player1.getPieces()[i].getYPosition();
            x = player1.getPieces()[i].getXPosition();
            if (y==newYTo){
                if (x==newXTo){
                    moveImg = player1.getPieces()[i].getImage();
                    player1.getPieces()[i].setXPosition(-100);
                    player1.getPieces()[i].setYPosition(-100);
                    gc.setFill(chessBoard.getSquares()[newXTo][newYTo].getColor());
                    gc.fillRect(newXTo*75,newYTo*75,75,75);
                    updateDeadXYPosition(moveImg);
                }
            }
        }
    }

    public void updateDeadXYPosition(Image moveImg){
        GraphicsContext df = deadField.getGraphicsContext2D();
        if (deady < 1){
            if (deadDidRun ==false){
                deadDidRun =true;
            }
            else {
                deadx+=1;
            }

            if (deadx == 4 ){
                deady+=1;
                deadx+=1;
                deadDidRun = false;
            }
        }
        if (deady == 1){

            if (deadx == 5){
                deadx = 0;
            }
            if (deadDidRun ==false){
                deadDidRun =true;
            }
            else {
                deadx+=1;
            }
            if (deadx ==4){
                deady+=1;
                deadx+=1;
                deadDidRun = false;
            }
        }
        if (deady == 2){

            if (deadx == 5){
                deadx = 0;
            }
            if (deadDidRun ==false){
                deadDidRun =true;
            }
            else {
                deadx+=1;
            }
            if (deadx ==4){
                deady+=1;
                deadx+=1;
                deadDidRun = false;
            }
        }
        if (deady == 3){

            if (deadx == 5){
                deadx = 0;
            }
            if (deadDidRun ==false){
                deadDidRun =true;
            }
            else {
                deadx+=1;
            }
            if (deadx ==4){
                deady+=1;
                deadx+=1;
                deadDidRun = false;
            }
        }
        if (deady == 4){

            if (deadx == 5){
                deadx = 0;
            }
            if (deadDidRun ==false){
                deadDidRun =true;
            }
            else {
                deadx+=1;
            }
            if (deadx ==4){
                deady+=1;
                deadx+=1;
                deadDidRun = false;
            }
        }
        if (deady == 5){

            if (deadx == 5){
                deadx = 0;
            }
            if (deadDidRun ==false){
                deadDidRun =true;
            }
            else {
                deadx+=1;
            }
            if (deadx ==4){
                deady+=1;
                deadx+=1;
                deadDidRun = false;
            }
        }
        if (deady == 6){

            if (deadx == 5){
                deadx = 0;
            }
            if (deadDidRun ==false){
                deadDidRun =true;
            }
            else {
                deadx+=1;
            }
            if (deadx ==4){
                deady+=1;
                deadx+=1;
                deadDidRun = false;
            }
        }
        if (deady == 7){

            if (deadx == 5){
                deadx = 0;
            }
            if (deadDidRun ==false){
                deadDidRun =true;
            }
            else {
                deadx+=1;
            }
            if (deadx ==4){
                deady+=1;
                deadx+=1;
                deadDidRun = false;
            }
        }
        if (deady == 8){

            if (deadx == 5){
                deadx = 0;
            }
            if (deadDidRun ==false){
                deadDidRun =true;
            }
            else {
                deadx+=1;
            }
            if (deadx ==4){
                deady+=1;
                deadx+=1;
                deadDidRun = false;
            }
        }
        if (deady == 9){

            if (deadx == 5){
                deadx = 0;
            }
            if (deadDidRun ==false){
                deadDidRun =true;
            }
            else {
                deadx+=1;
            }
            if (deadx ==4){
                deady+=1;
                deadx+=1;
                deadDidRun = false;
            }
        }
        df.drawImage(moveImg,deadx*50,deady*50);
    }
}