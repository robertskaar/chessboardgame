package Domain;

import javafx.scene.paint.Color;

public class Square {
    private final int SIZE = 75;
    private Color color;

    public boolean isOccupied() {
        return occupied;
    }

    public void setOccupied(boolean occupied) {
        this.occupied = occupied;
    }

    private boolean occupied = false;

    public int getSIZE() {
        return SIZE;
    }

    public Color getColor() {
        return color;
    }

    public void setColor(Color color) {
        this.color = color;
    }
}
