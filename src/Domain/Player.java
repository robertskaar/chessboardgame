package Domain;

import javafx.scene.image.Image;
import javafx.scene.paint.Color;

public class Player {
    // sets the White pieces image
    private Image whitePawn = new Image("Presentation/ChessPiece_WhitePawn.png");
    private Image whiteHorse = new Image("Presentation/ChessPiece_WhiteHorse.png");
    private Image whiteBishop = new Image("Presentation/ChessPiece_WhiteBishop.png");
    private Image whiteTower = new Image("Presentation/ChessPiece_WhiteTower.png");
    private Image whiteQueen = new Image("Presentation/ChessPiece_WhiteQueen.png");
    private Image whiteKing = new Image("Presentation/ChessPiece_WhiteKing.png");

    // sets the Black pieces image
    private Image blackPawn = new Image("Presentation/ChessPiece_BlackPawn.png");
    private Image blackHorse = new Image("Presentation/ChessPiece_BlackHorse.png");
    private Image blackBishop = new Image("Presentation/ChessPiece_BlackBishop.png");
    private Image blackTower = new Image("Presentation/ChessPiece_BlackTower.png");
    private Image blackQueen = new Image("Presentation/ChessPiece_BlackQueen.png");
    private Image blackKing = new Image("Presentation/ChessPiece_BlackKing.png");


    public Piece[] getPieces() {
        return pieces;
    }

    // pieces variable array declared
    private Piece[] pieces = new Piece[16];

    // constructor for player, receiving a color
    public Player(Color color) {
        for (int i = 0; i < 16; i++) {
            pieces[i] = new Piece();
            if (color.equals(Color.WHITE)) {
                if (i == 15) {
                    pieces[i].setImage(whiteKing);
                    pieces[i].setMoves(1);
                    pieces[i].setCanMoveCross(true);
                    pieces[i].setCanMoveStraight(true);
                    pieces[i].setCanMoveBack(true);
                    pieces[i].setCanMoveSides(true);
                    pieces[i].setXPosition(4);
                    pieces[i].setYPosition(7);
                } else if (i == 14) {
                    pieces[i].setImage(whiteQueen);
                    pieces[i].setMoves(8);
                    pieces[i].setCanMoveCross(true);
                    pieces[i].setCanMoveStraight(true);
                    pieces[i].setCanMoveBack(true);
                    pieces[i].setCanMoveSides(true);
                    pieces[i].setXPosition(3);
                    pieces[i].setYPosition(7);
                } else if (i == 13) {
                    pieces[i].setImage(whiteBishop);
                    pieces[i].setMoves(8);
                    pieces[i].setCanMoveCross(true);
                    pieces[i].setCanMoveStraight(false);
                    pieces[i].setCanMoveBack(true);
                    pieces[i].setCanMoveSides(false);
                    pieces[i].setXPosition(2);
                    pieces[i].setYPosition(7);
                } else if (i == 12) {
                    pieces[i].setImage(whiteBishop);
                    pieces[i].setMoves(8);
                    pieces[i].setCanMoveCross(true);
                    pieces[i].setCanMoveStraight(false);
                    pieces[i].setCanMoveBack(true);
                    pieces[i].setCanMoveSides(false);
                    pieces[i].setXPosition(5);
                    pieces[i].setYPosition(7);
                } else if (i == 11) {
                    pieces[i].setImage(whiteHorse);
                    pieces[i].setMoves(3);
                    pieces[i].setCanMoveCross(false);
                    pieces[i].setCanMoveStraight(false);
                    pieces[i].setCanMoveBack(false);
                    pieces[i].setCanMoveSides(false);
                    pieces[i].setCanMoveHorse(true);
                    pieces[i].setXPosition(1);
                    pieces[i].setYPosition(7);
                } else if (i == 10) {
                    pieces[i].setImage(whiteHorse);
                    pieces[i].setMoves(3);
                    pieces[i].setCanMoveCross(false);
                    pieces[i].setCanMoveStraight(false);
                    pieces[i].setCanMoveBack(false);
                    pieces[i].setCanMoveSides(false);
                    pieces[i].setCanMoveHorse(true);
                    pieces[i].setXPosition(6);
                    pieces[i].setYPosition(7);
                } else if (i == 9) {
                    pieces[i].setImage(whiteTower);
                    pieces[i].setMoves(8);
                    pieces[i].setCanMoveCross(false);
                    pieces[i].setCanMoveStraight(true);
                    pieces[i].setCanMoveBack(true);
                    pieces[i].setCanMoveSides(true);
                    pieces[i].setXPosition(0);
                    pieces[i].setYPosition(7);
                } else if (i == 8) {
                    pieces[i].setImage(whiteTower);
                    pieces[i].setMoves(8);
                    pieces[i].setCanMoveCross(false);
                    pieces[i].setCanMoveStraight(true);
                    pieces[i].setCanMoveBack(true);
                    pieces[i].setCanMoveSides(true);
                    pieces[i].setXPosition(7);
                    pieces[i].setYPosition(7);
                }
                else if (i==7){
                    pieces[i].setImage(whitePawn);
                    pieces[i].setMoves(1);
                    pieces[i].setCanMoveCross(false);
                    pieces[i].setCanMoveStraight(true);
                    pieces[i].setCanMoveBack(false);
                    pieces[i].setCanMoveSides(false);
                    pieces[i].setXPosition(7);
                    pieces[i].setYPosition(6);
                }
                else if (i==6){
                    pieces[i].setImage(whitePawn);
                    pieces[i].setMoves(1);
                    pieces[i].setCanMoveCross(false);
                    pieces[i].setCanMoveStraight(true);
                    pieces[i].setCanMoveBack(false);
                    pieces[i].setCanMoveSides(false);
                    pieces[i].setXPosition(6);
                    pieces[i].setYPosition(6);
                }
                else if (i==5){
                    pieces[i].setImage(whitePawn);
                    pieces[i].setMoves(1);
                    pieces[i].setCanMoveCross(false);
                    pieces[i].setCanMoveStraight(true);
                    pieces[i].setCanMoveBack(false);
                    pieces[i].setCanMoveSides(false);
                    pieces[i].setXPosition(5);
                    pieces[i].setYPosition(6);
                }
                else if (i==4){
                    pieces[i].setImage(whitePawn);
                    pieces[i].setMoves(1);
                    pieces[i].setCanMoveCross(false);
                    pieces[i].setCanMoveStraight(true);
                    pieces[i].setCanMoveBack(false);
                    pieces[i].setCanMoveSides(false);
                    pieces[i].setXPosition(4);
                    pieces[i].setYPosition(6);
                }
                else if (i==3){
                    pieces[i].setImage(whitePawn);
                    pieces[i].setMoves(1);
                    pieces[i].setCanMoveCross(false);
                    pieces[i].setCanMoveStraight(true);
                    pieces[i].setCanMoveBack(false);
                    pieces[i].setCanMoveSides(false);
                    pieces[i].setXPosition(3);
                    pieces[i].setYPosition(6);
                }
                else if (i==2){
                    pieces[i].setImage(whitePawn);
                    pieces[i].setMoves(1);
                    pieces[i].setCanMoveCross(false);
                    pieces[i].setCanMoveStraight(true);
                    pieces[i].setCanMoveBack(false);
                    pieces[i].setCanMoveSides(false);
                    pieces[i].setXPosition(2);
                    pieces[i].setYPosition(6);
                }
                else if (i==1){
                    pieces[i].setImage(whitePawn);
                    pieces[i].setMoves(1);
                    pieces[i].setCanMoveCross(false);
                    pieces[i].setCanMoveStraight(true);
                    pieces[i].setCanMoveBack(false);
                    pieces[i].setCanMoveSides(false);
                    pieces[i].setXPosition(1);
                    pieces[i].setYPosition(6);
                }
                else {
                    pieces[i].setImage(whitePawn);
                    pieces[i].setMoves(1);
                    pieces[i].setCanMoveCross(false);
                    pieces[i].setCanMoveStraight(true);
                    pieces[i].setCanMoveBack(false);
                    pieces[i].setCanMoveSides(false);
                    pieces[i].setXPosition(0);
                    pieces[i].setYPosition(6);
                }
            }
            if (color.equals(Color.BLACK)) {
                if (i == 15) {
                    pieces[i].setImage(blackKing);
                    pieces[i].setMoves(1);
                    pieces[i].setCanMoveCross(true);
                    pieces[i].setCanMoveStraight(true);
                    pieces[i].setCanMoveBack(true);
                    pieces[i].setCanMoveSides(true);
                    pieces[i].setXPosition(4);
                    pieces[i].setYPosition(0);
                } else if (i == 14) {
                    pieces[i].setImage(blackQueen);
                    pieces[i].setMoves(8);
                    pieces[i].setCanMoveCross(true);
                    pieces[i].setCanMoveStraight(true);
                    pieces[i].setCanMoveBack(true);
                    pieces[i].setCanMoveSides(true);
                    pieces[i].setXPosition(3);
                    pieces[i].setYPosition(0);
                } else if (i == 13) {
                    pieces[i].setImage(blackBishop);
                    pieces[i].setMoves(8);
                    pieces[i].setCanMoveCross(true);
                    pieces[i].setCanMoveStraight(false);
                    pieces[i].setCanMoveBack(true);
                    pieces[i].setCanMoveSides(false);
                    pieces[i].setXPosition(2);
                    pieces[i].setYPosition(0);
                } else if (i == 12) {
                    pieces[i].setImage(blackBishop);
                    pieces[i].setMoves(8);
                    pieces[i].setCanMoveCross(true);
                    pieces[i].setCanMoveStraight(false);
                    pieces[i].setCanMoveBack(true);
                    pieces[i].setCanMoveSides(false);
                    pieces[i].setXPosition(5);
                    pieces[i].setYPosition(0);
                } else if (i == 11) {
                    pieces[i].setImage(blackHorse);
                    pieces[i].setMoves(3);
                    pieces[i].setCanMoveCross(false);
                    pieces[i].setCanMoveStraight(false);
                    pieces[i].setCanMoveBack(false);
                    pieces[i].setCanMoveSides(false);
                    pieces[i].setCanMoveHorse(true);
                    pieces[i].setXPosition(1);
                    pieces[i].setYPosition(0);
                } else if (i == 10) {
                    pieces[i].setImage(blackHorse);
                    pieces[i].setMoves(3);
                    pieces[i].setCanMoveCross(false);
                    pieces[i].setCanMoveStraight(false);
                    pieces[i].setCanMoveBack(false);
                    pieces[i].setCanMoveSides(false);
                    pieces[i].setCanMoveHorse(true);
                    pieces[i].setXPosition(6);
                    pieces[i].setYPosition(0);
                } else if (i == 9) {
                    pieces[i].setImage(blackTower);
                    pieces[i].setMoves(8);
                    pieces[i].setCanMoveCross(false);
                    pieces[i].setCanMoveStraight(true);
                    pieces[i].setCanMoveBack(true);
                    pieces[i].setCanMoveSides(true);
                    pieces[i].setXPosition(0);
                    pieces[i].setYPosition(0);
                } else if (i == 8) {
                    pieces[i].setImage(blackTower);
                    pieces[i].setMoves(8);
                    pieces[i].setCanMoveCross(false);
                    pieces[i].setCanMoveStraight(true);
                    pieces[i].setCanMoveBack(true);
                    pieces[i].setCanMoveSides(true);
                    pieces[i].setXPosition(7);
                    pieces[i].setYPosition(0);
                }
                else if (i==7){
                    pieces[i].setImage(blackPawn);
                    pieces[i].setMoves(1);
                    pieces[i].setCanMoveCross(false);
                    pieces[i].setCanMoveStraight(true);
                    pieces[i].setCanMoveBack(false);
                    pieces[i].setCanMoveSides(false);
                    pieces[i].setXPosition(7);
                    pieces[i].setYPosition(1);
                }
                else if (i==6){
                    pieces[i].setImage(blackPawn);
                    pieces[i].setMoves(1);
                    pieces[i].setCanMoveCross(false);
                    pieces[i].setCanMoveStraight(true);
                    pieces[i].setCanMoveBack(false);
                    pieces[i].setCanMoveSides(false);
                    pieces[i].setXPosition(6);
                    pieces[i].setYPosition(1);
                }
                else if (i==5){
                    pieces[i].setImage(blackPawn);
                    pieces[i].setMoves(1);
                    pieces[i].setCanMoveCross(false);
                    pieces[i].setCanMoveStraight(true);
                    pieces[i].setCanMoveBack(false);
                    pieces[i].setCanMoveSides(false);
                    pieces[i].setXPosition(5);
                    pieces[i].setYPosition(1);
                }
                else if (i==4){
                    pieces[i].setImage(blackPawn);
                    pieces[i].setMoves(1);
                    pieces[i].setCanMoveCross(false);
                    pieces[i].setCanMoveStraight(true);
                    pieces[i].setCanMoveBack(false);
                    pieces[i].setCanMoveSides(false);
                    pieces[i].setXPosition(4);
                    pieces[i].setYPosition(1);
                }
                else if (i==3){
                    pieces[i].setImage(blackPawn);
                    pieces[i].setMoves(1);
                    pieces[i].setCanMoveCross(false);
                    pieces[i].setCanMoveStraight(true);
                    pieces[i].setCanMoveBack(false);
                    pieces[i].setCanMoveSides(false);
                    pieces[i].setXPosition(3);
                    pieces[i].setYPosition(1);
                }
                else if (i==2){
                    pieces[i].setImage(blackPawn);
                    pieces[i].setMoves(1);
                    pieces[i].setCanMoveCross(false);
                    pieces[i].setCanMoveStraight(true);
                    pieces[i].setCanMoveBack(false);
                    pieces[i].setCanMoveSides(false);
                    pieces[i].setXPosition(2);
                    pieces[i].setYPosition(1);
                }
                else if (i==1){
                    pieces[i].setImage(blackPawn);
                    pieces[i].setMoves(1);
                    pieces[i].setCanMoveCross(false);
                    pieces[i].setCanMoveStraight(true);
                    pieces[i].setCanMoveBack(false);
                    pieces[i].setCanMoveSides(false);
                    pieces[i].setXPosition(1);
                    pieces[i].setYPosition(1);
                }
                else {
                    pieces[i].setImage(blackPawn);
                    pieces[i].setMoves(1);
                    pieces[i].setCanMoveCross(false);
                    pieces[i].setCanMoveStraight(true);
                    pieces[i].setCanMoveBack(false);
                    pieces[i].setCanMoveSides(false);
                    pieces[i].setXPosition(0);
                    pieces[i].setYPosition(1);
                }

            }
        }
    }
}
