package Domain;

import javafx.scene.image.Image;


public class Piece {
    private Image image;
    private int moves;
    private boolean canMoveStraight = false;
    private boolean canMoveCross = false;
    private boolean canMoveBack = false;
    private boolean canMoveSides = false;
    private boolean canMoveHorse = false;
    private int XPosition;
    private int YPosition;

    public boolean isCanMoveHorse() {
        return canMoveHorse;
    }

    public void setCanMoveHorse(boolean canMoveHorse) {
        this.canMoveHorse = canMoveHorse;
    }

    public boolean isCanMoveBack() {
        return canMoveBack;
    }

    public void setCanMoveBack(boolean canMoveBack) {
        this.canMoveBack = canMoveBack;
    }

    public boolean isCanMoveSides() {
        return canMoveSides;
    }

    public void setCanMoveSides(boolean canMoveSides) {
        this.canMoveSides = canMoveSides;
    }

    public int getXPosition() {
        return XPosition;
    }

    public void setXPosition(int XPosition) {
        this.XPosition = XPosition;
    }

    public int getYPosition() {
        return YPosition;
    }

    public void setYPosition(int YPosition) {
        this.YPosition = YPosition;
    }

    public boolean isCanMoveStraight() {
        return canMoveStraight;
    }

    public void setCanMoveStraight(boolean canMoveStraight) {
        this.canMoveStraight = canMoveStraight;
    }

    public boolean isCanMoveCross() {
        return canMoveCross;
    }

    public void setCanMoveCross(boolean canMoveCross) {
        this.canMoveCross = canMoveCross;
    }


    public Image getImage() {
        return image;
    }

    public void setImage(Image image) {
        this.image = image;
    }

    public int getMoves() {
        return moves;
    }

    public void setMoves(int moves) {
        this.moves = moves;
    }
}
