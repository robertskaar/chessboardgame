package Domain;

import javafx.scene.paint.Color;

public class Board {

    public Square[][] getSquares() {
        return squares;
    }

    private Square[][] squares = new Square[8][8];

    public Board() {

        for (int i = 0; i < 8; i++) {
            for (int j = 0; j < 8; j++) {
                squares[i][j] = new Square();
                if (i % 2 == 0) {
                    if (j % 2 == 0)
                        squares[i][j].setColor(Color.WHITE);
                    else {
                        squares[i][j].setColor(Color.BROWN);
                    }
                } else {
                    if (j % 2 == 0)
                        squares[i][j].setColor(Color.BROWN);
                    else {
                        squares[i][j].setColor(Color.WHITE);
                    }
                }
            }
        }
    }
}
